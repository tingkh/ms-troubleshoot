
package org.mule.cm.training.experince.datamodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "documentName",
    "documentLocation",
    "docId",
    "documentProcessStatus"
})
public class Document {

    @JsonProperty("documentType")
    private String documentType;
    @JsonProperty("documentName")
    private String documentName;
    @JsonProperty("docId")
    private String docId;
    @JsonProperty("documentProcessStatus")
    private String documentProcessStatus;

//    @JsonIgnore
//    private Map<String, Object> additionalProperties = new HashMap<String, Object>();



	@JsonProperty("documentType")
    public String getDocumentType() {
        return documentType;
    }

    @JsonProperty("documentType")
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    @JsonProperty("documentName")
    public String getDocumentName() {
        return documentName;
    }

    @JsonProperty("documentName")
    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    @JsonProperty("docId")
    public String getDocId() {
        return docId;
    }

    @JsonProperty("docId")
    public void setDocId(String docId) {
        this.docId = docId;
    }
    
    @JsonProperty("documentProcessStatus")
    public String getDocumentProcessStatus()
    {
    		return this.documentProcessStatus;
    }
    
    @JsonProperty("documentProcessStatus")
    public void setDocumentProcessStatus(String documentProcessStatus)
    {
    		this.documentProcessStatus=documentProcessStatus;
    }
//    @JsonAnyGetter
//    public Map<String, Object> getAdditionalProperties() {
//        return this.additionalProperties;
//    }
//
//    @JsonAnySetter
//    public void setAdditionalProperty(String name, Object value) {
//        this.additionalProperties.put(name, value);
//    }

}
