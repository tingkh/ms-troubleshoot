package org.mule.cm.training.processing;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.cm.training.exception.DocumentFileNotFoundException;
import org.mule.cm.training.exception.PayloadNullException;

public class DocumentFileNotFoundProcessingException  implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		// TODO Auto-generated method stub
		//return null;
		throw new DocumentFileNotFoundException("Document File Could Not Be Found"); 
	}

}
