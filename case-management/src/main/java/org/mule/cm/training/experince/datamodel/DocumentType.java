
package org.mule.cm.training.experince.datamodel;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"submissionNo",
    "countryUploaded",
    "policyNo",
    "documents",
    "customer"
})
public class DocumentType {

    @JsonProperty("countryUploaded")
    private String countryUploaded;
    @JsonProperty("policyNo")
    private String policyNo;
    @JsonProperty("documents")
    private List<Document> documents = null;
    @JsonProperty("customer")
    private CustomerType customer;
    @JsonProperty("submissionNo")
    private String submissionNo;
//    @JsonIgnore
//    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("countryUploaded")
    public String getCountryUploaded() {
        return countryUploaded;
    }

    @JsonProperty("countryUploaded")
    public void setCountryUploaded(String countryUploaded) {
        this.countryUploaded = countryUploaded;
    }

    @JsonProperty("policyNo")
    public String getPolicyNo() {
        return policyNo;
    }

    @JsonProperty("policyNo")
    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    @JsonProperty("documents")
    public List<Document> getDocuments() {
        return documents;
    }

    @JsonProperty("documents")
    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }
    
    @JsonProperty("customer")
    public CustomerType getCustomer() {
		return customer;
	}

    @JsonProperty("customer")
	public void setCustomer(CustomerType customer) {
		this.customer = customer;
	}

    @JsonProperty("submissionNo")
	public String getSubmissionNo() {
		return submissionNo;
	}

    @JsonProperty("submissionNo")
	public void setSubmissionNo(String submissionNo) {
		this.submissionNo = submissionNo;
	}
    
    

//    @JsonAnyGetter
//    public Map<String, Object> getAdditionalProperties() {
//        return this.additionalProperties;
//    }
//
//    @JsonAnySetter
//    public void setAdditionalProperty(String name, Object value) {
//        this.additionalProperties.put(name, value);
//    }

}
