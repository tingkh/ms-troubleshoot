package org.mule.cm.training.experince.datamodel;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

//CustomerType:
//	  properties:
//	    nric: string
//	    firstName?: string
//	    lastName?: string
//	    email? : string 
	    		
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "nric",
    "firstName",
    "lastName",
    "email"
})

public class CustomerType {

	@JsonProperty("nric")
    private String nric;
    
    @JsonProperty("firstName")
    private String firstName;
    
    @JsonProperty("lastName")
    private String lastName;
    
    @JsonProperty("email")
    private String email;

    @JsonProperty("nric")
	public String getNric() {
		return nric;
	}

    @JsonProperty("nric")
	public void setNric(String nric) {
		this.nric = nric;
	}

	@JsonProperty("firstName")
	public String getFirstName() {
		return firstName;
	}
	
	@JsonProperty("firstName")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@JsonProperty("lastName")
	public String getLastName() {
		return lastName;
	}
	
	@JsonProperty("lastName")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}
    

}
