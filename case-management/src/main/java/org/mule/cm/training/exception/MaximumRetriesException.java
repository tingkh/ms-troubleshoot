package org.mule.cm.training.exception;

public class MaximumRetriesException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1123544523432L;
	String mistake;
	
	public MaximumRetriesException() {
		super();
		mistake = "test";
	}
	
	public MaximumRetriesException(String err) {
		super(err);
		mistake = err;
	}
	
	 public String getError()
	  {
	    return mistake;
	  }
}
