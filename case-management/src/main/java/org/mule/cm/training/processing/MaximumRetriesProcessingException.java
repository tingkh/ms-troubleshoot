package org.mule.cm.training.processing;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;
import org.mule.cm.training.exception.MaximumRetriesException;
import org.mule.cm.training.exception.PayloadNullException;

public class MaximumRetriesProcessingException  implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		// TODO Auto-generated method stub
		//return null;
		throw new MaximumRetriesException("Maximum Number Of Retries Reached");
	}

}
