package org.mule.cm.training.exception;

public class PayloadNullException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1123544523432L;
	String mistake;
	
	public PayloadNullException() {
		super();
		mistake = "test";
	}
	
	public PayloadNullException(String err) {
		super(err);
		mistake = err;
	}
	
	 public String getError()
	  {
	    return mistake;
	  }
}
