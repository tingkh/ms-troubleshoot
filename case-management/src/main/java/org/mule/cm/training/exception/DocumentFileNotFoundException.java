package org.mule.cm.training.exception;

public class DocumentFileNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1123544523432L;
	String mistake;
	
	public DocumentFileNotFoundException() {
		super();
		mistake = "test";
	}
	
	public DocumentFileNotFoundException(String err) {
		super(err);
		mistake = err;
	}
	
	 public String getError()
	  {
	    return mistake;
	  }
}
