package org.mule.cm.training.processing;

import java.util.UUID;

public class CaseManagementProcessAPI {
	public String genSubmissionUniqueCode()
	{
		return UUID.randomUUID().toString();
	}
}
